# Baixando o Repositório do Git
## Logar na maquina virtual

```bash
ssh <user>@<ip>
```

## Criar uma pasta para o projeto

```bash
mkdir docker-exercise && cd docker-exercise
```

## Clonar o repositório

```bash
git clone  https://gitlab.com/opytaon/public/treinamento-docker/docker-sample
```

## Entrar na pasta do projeto

```bash 
cd docker-sample
```

## Compilar os fontes com o Maven

```bash
mvn clean package
```

Caso não tenha o Maven Instalado, siga os passos abaixo:

```bash
sudo apt install maven
```

## Crie o arquivo Dockerfile

## Construa a imagem

```bash
docker build -t docker-sample .
```

## Listar as imagens

```bash
    docker images
```

## Rodar o container

```bash
docker run -p 8080:8080 docker-sample
```

## Listar os containers

```bash
docker ps -a
```
## Rodar como daemon
```bash 
docker run -d -p 8080:8080 docker-sample
```

## Acessar o container

```bash
docker exec -it  <container_id> /bin/sh
```
## Verificar o log do container

```bash
docker logs  [-f] <container_id>
```

## Parar o container

```bash 
    docker stop <container_id>
```

## Remover o container
```bash
    docker rm <container_id>
```

## Remover a imagem
```bash
    docker rmi <image_id>
```

# Docker Compose

## Criar o arquivo docker-compose.yml

```yml  
version: '3.1'
