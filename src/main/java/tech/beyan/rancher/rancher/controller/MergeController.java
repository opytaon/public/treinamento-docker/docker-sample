package tech.beyan.rancher.rancher.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import tech.beyan.rancher.rancher.util.MergeSort;

@RestController
@Slf4j
public class MergeController {
    Random rnd = new java.util.Random();
    MergeSort<Integer> ms = new MergeSort<>();
    String basePath = Paths.get(".").toAbsolutePath().normalize().toString();

    @GetMapping("/env")
    public ResponseEntity<String> env() {
        String toDisplaey = System.getenv("EXEMPLO_VARIAVEL");

        if (toDisplaey == null) {
            toDisplaey = "No environment variable set";
        }

        return ResponseEntity.ok(toDisplaey);
    }

    @GetMapping("/mergesort/{quantidade}")
    public ResponseEntity<Integer> mergeSorteRequest(@PathVariable(value = "quantidade") Integer quantidade) {
        // generate a list of random numbers using the Random class
        log.info("MergeSort request received with {} elements", quantidade);
        List<Integer> unsorted = new ArrayList<>();

        for (int i = 0; i < quantidade; i++) {
            unsorted.add(rnd.nextInt(100000));
        }

        // menssure time before sorting
        long startTime = System.nanoTime();

        // var sortedObjects =
        ms.mergeSort(unsorted);

        long endTime = System.nanoTime();
        long duration = (endTime - startTime) / 1000000; // divide by 1000000 to get milliseconds.
        log.info("MergeSort request completed in {} ms", duration);

        saveToFile(basePath + "/lista/sorted.txt", unsorted);
        return ResponseEntity.ok(Math.toIntExact(duration));

    }

    // Write a number of random integer values and sort them using the merge sort
    // algorithm.

    private void saveToFile(String filename, List<Integer> list) {
        try {
            // if folder does not exist, create it
            java.nio.file.Files.createDirectories(Paths.get(filename).getParent());

            PrintWriter writer = new PrintWriter(filename, "UTF-8");
            for (Integer i : list) {
                writer.print(i);
                writer.print(" ");
            }

            writer.println("----------------");

            writer.close();
        } catch (IOException e) {
            log.error("Error saving to file", e);
        }
    }

}
